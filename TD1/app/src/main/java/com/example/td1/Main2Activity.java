package com.example.td1;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

public class Main2Activity extends AppCompatActivity {

    private EditText etGauche;
    private EditText etDroit;
    private TextView tvResult;
    private Button resultButton;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main2);
        etGauche =  findViewById(R.id.FirstNumber);
        etDroit =  findViewById(R.id.SecondNumber);
        tvResult =  findViewById(R.id.resultId);

        resultButton =  findViewById(R.id.SignEgal);
        resultButton.setOnClickListener(
                new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        ComputeAdd(v);
                    }
                }
        );
    }

    public void ComputeAdd(View v)
    {
        int a = Integer.parseInt(etGauche.getText().toString());
        int b = Integer.parseInt(etDroit.getText().toString());
        int somme = a+b;
        tvResult.setText("La somme est la suivante : "+somme);
    }


}
